from pydantic import BaseModel


class ToDo(BaseModel):
    id: int
    title: str
    is_complete: bool

    class Config:
        orm_mode = True
