from sqlalchemy.ext.declarative import declarative_base
import sqlalchemy as sa


Base = declarative_base()


class ToDo(Base):
    __tablename__ = 'todos'

    id = sa.Column(sa.Integer, primary_key=True)
    title = sa.Column(sa.String, unique=True)
    is_complete = sa.Column(sa.Boolean, default=False)
