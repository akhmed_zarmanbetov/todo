from pydantic import BaseSettings


class Settings(BaseSettings):
    database_url: str = 'sqlite:///./sql_app.db'
    app_name: str = 'ToDo список'


settings = Settings()
