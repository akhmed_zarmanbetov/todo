from fastapi import FastAPI, Depends, Request, Form
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi.responses import RedirectResponse
from starlette import status as st

from sqlalchemy.orm import Session

from todo.database import engine, get_session
from todo import schema, models
from todo.config import settings



models.Base.metadata.create_all(engine)
app = FastAPI()
app.mount('/static', StaticFiles(directory='static'), name='static')
templates = Jinja2Templates(directory='templates')


@app.get('/')
async def home(request: Request, session: Session = Depends(get_session)):
    todos = session.query(models.ToDo).all()

    return templates.TemplateResponse(
        'index.html',
        {
            'request': request,
            'app_name': settings.app_name,
            'todo_list': todos,
        }
    )


@app.post("/add")
async def add(title: str =  Form(...), session: Session = Depends(get_session)):

    new_todo = models.ToDo(title=title)

    session.add(new_todo)
    session.commit()
    session.refresh(new_todo)

    url = app.url_path_for('home')

    return RedirectResponse(url=url, status_code=st.HTTP_303_SEE_OTHER)


@app.get('/update/{todo_id}')
async def update(todo_id: int,  session: Session = Depends(get_session)):
    todo = session.query(models.ToDo).filter_by(id=todo_id).first()
    todo.is_complete = not todo.is_complete
    session.commit()
    session.refresh(todo)

    url = app.url_path_for('home')

    return RedirectResponse(url=url, status_code=st.HTTP_303_SEE_OTHER)


@app.get('/delete/{todo_id}')
async def delete(todo_id: int,  session: Session = Depends(get_session)):
    todo = session.query(models.ToDo).filter_by(id=todo_id).first()

    session.delete(todo)
    session.commit()

    url = app.url_path_for('home')

    return RedirectResponse(url=url, status_code=st.HTTP_303_SEE_OTHER)
